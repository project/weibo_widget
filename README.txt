INTRODUCTION
------------

The module displays a block with the main Weibo Widgets.
Configure the Appkey from the page with you want to use.
The module will provide one block per widget.

REQUIREMENTS
------------

None.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

 * There are 1 place you have to configure Weibo:

   - Administration » Configuration » Web Services » Weibo Widget:

     This is the place where the Weibo API will be made.

MAINTAINERS
-----------

Current maintainers:
 * Adriano Pulz (adrianopulz) - https://www.drupal.org/user/1327214
